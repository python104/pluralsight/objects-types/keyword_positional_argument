def banner(message, border='-'):
    line = border * len(message)
    print(line)
    print(message)
    print(line,"\n")


banner("this is test message") #don't need to provide border string as we have provided a default value
banner("this is second test message", '#') # providing optional argument (not suited for production code)
banner("this is third test message", border='*') 
# 1. specifying the argument name at the call site (best practice)
# 2. above is also called "Keyword argument", the former argument is called positional argument
    # a. message = positional argument
    # b. border='*' Keyword argument matched by name
# 3. All keyword argument must be specified after the positional argument
banner(message="this is Fourth test message", border='%') 
banner( border='$', message="this is Fifth test message")
# 4. If we specify each argument by keyword (name), we have the liberty to specify arguments in any order

